#include <stdio.h>
#define SIZE 15

//Prototipo 
void impNombre(const char nom[], unsigned int n);

int main(void){

	// Declaro cadena de caracteres.
	char nombre[SIZE];

	// Pregunto a usuario por nombre
	printf("Ingrese nombre: ");
	// Obtengo nombre de usuario
	scanf("%s", nombre);

	// Imprimo arreglo con índices
	impNombre(nombre, SIZE);

}


// Entradas:
// - nom: Arreglo de caracteres de solo lectura.
// - n: tamaño del arreglo.
// Salidas:
// - Ninguna.
void impNombre(const char nom[], unsigned int n){
	// Inicio línea de caracteres
	printf("%s", "Caracteres |");

	// Recorro nom desde i = 0, hasta i menor a n (tamaño).
	for (unsigned int i = 0; i < n; i++){
		// Si elemento es diferente de caracter NULL
		if (nom[i] != 0){
			// Imprimo caracter
			printf("%2c|", nom[i]);
		}
		// De lo contrario, imprimo secuencia \0
		else{
			printf("%s", "\\0|");
		}
	}

	// Imprimo línea de índices
	printf("\n%s", "Índices    |");
	for (unsigned int i = 0; i < n; i++){
		printf("%2d|", i);
	}

	// Agrego cambio de línea
	puts("");
}
